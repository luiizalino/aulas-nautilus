
class Participante:
  def __init__(self, nome, vitorias, derrotas, dragoes, baroes):
    self.nome = nome
    self.vitorias = vitorias
    self.derrotas = derrotas
    self.pontos = self.vitorias * 3
    self.dragoes = dragoes
    self.baroes = baroes
    self._posicao = 1

  def __repr__(self):
    return f"""----------
    Time: {self.nome}
    Pontuação: {self.pontos}
    Vitórias: {self.vitorias}
    Derrotas: {self.derrotas}
    Dragões: {self.dragoes}
    Barões: {self.baroes}
    """

  def __gt__(self, outro):
    if self.pontos > outro.pontos:
      return True
    else:
      return False
  
  def __lt__(self, outro):
    if self.pontos < outro.pontos:
      return True
    else:
      return False

  def __eq__(self, outro):
    if self.pontos == outro.pontos:
      return True
    else:
      return False

  def __add__(self, outro):
    return self.pontos + outro.pontos
  
  def __sub__(self, outro):
    return self.pontos - outro.pontos

pain = Participante('paIN', 0, 0, 20, 3)
cnb = Participante('CNB', 50, 1, 17, 2)
keyd = Participante('KEYD', 27, 2, 15, 1)
red = Participante('Red Canids', 60, 3, 7, 0)

times = []
times.append(pain)
times.append(cnb)
times.append(keyd)
times.append(red)

for time in times:
  for time2 in times:
    if time < time2:
      time._posicao += 1

print('|  Posição   |   Nome   |   Pontos  |   Vitórias  |   Derrotas  |   Dragões |   Barões  |')
for time in times:
  print(
    f"""
    | {time._posicao} |  {time.nome}  |   {time.pontos}   |   {time.vitorias}   |   {time.derrotas}   |   {time.dragoes}    |   {time.baroes}   |
    """
  )
